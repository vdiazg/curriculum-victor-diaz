<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML,CSS,JavaScript">
  <meta name="author" content="Victor M. Diaz G.">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="shortcut icon" href="img/Web_VD.jpg">
  <title>CV - Víctor Díaz</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="fonts/css/font-awesome.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link href="css/custom_contacto_referencias.css" rel="stylesheet">
  <link href="css/particles-js.css" rel="stylesheet">
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>
  <div id="particles-js"></div>
  <div class="container mlr-10">

    <div class="row">
      <div class="col-xs-12">
        <div id="photo-header" class="text-center">
          <!-- PHOTO (AVATAR) -->
          <div id="photo">
            <img src="img/Web_VD.jpg" alt="avatar">
          </div>
          <div id="text-header">
            <h1>Hola,<br> Mi nombre es <span>VÍCTOR DÍAZ</span><!-- <sup>26years</sup> -->, Developer Full-Stack </h1>
          </div>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-xs-12 col-sm-7">

        <!-- Perfil Profecional-->
        <div class="box">
          <h2>¿ Quien Soy ?</h2>
          <!-- <p align="justify">Ingeniero Informático de DuocUC Sede de Viña del Mar con 4 años de experiencia profesional. Mis conocimientos y experiencia se relacionan con gestionar y participar en las distintas etapas del ciclo de desarrollo de software. Me encuentro capacitado para analizar, diseñar, desarrollar, implementar, gestionar y dar soporte a sistemas computacionales, velando por la seguridad y la calidad de los sistemas, aplicaciones e información. Adicionalmente, cuento con experiencia en la Gestión de Proyectos y Manejo de Equipos Interdisciplinarios. Tengo habilidad para relacionarme con las personas, soy organizado en mi trabajo, poseo un gran sentido de responsabilidad, constantemente me planteo retos y me gusta identificarme con los objetivos de la empresa donde desempeño mi trabajo y servicio.</p> -->
          <p align="justify">
            Ingeniero Informático con +5 años de experiencia como Desarrollador Full-Stack tanto en empresas tradicionales como en emprendimientos. 
          Siento que mi paso por el mundo del emprendimiento me ha convertido en un verdadero híbrido profesional, no solamente por estar al día con las tecnologías más innovadoras disponibles en el mercado, sino que también aprendiendo y aplicando innovadoras estrategias de ventas, marketing y en esencia, integrar la visión comercial en los proyectos. Esto me permite ser creativo al momento de enfrentarme con problemas, siempre tratando de encontrar la solución más beneficiosa en términos de tiempo y costo-beneficio para la empresa y el cliente.</p>
        </div>

        <!-- FORMACION ACADEMICA -->
        <div class="box">
          <h2>Formación Academica</h2>
          <p align="justify">Titulado el año 2015 en el Instituto Profesional DuocUC, Sede de Viña del Mar, como alumno destacado con dos honores por participación en el Centro de Innovación y Transferencia Tecnológica (CITT) de la sede. </p>
        </div>

        <!-- experiences -->
        <div class="box">
          <h2>Experiencias</h2>
          <div  style="">
            <ul id="education" class="clearfix">
              <li>
                <div class="year pull-left">2017</div>
                <div class="description pull-right">
                  <div class="row">
                    <div class="col-xs-10 col-md-11 mb--12">
                      <h3> Edoome Inc. </h3>
                      <p>Desarrollador Full-Stack </p>
                    </div>
                    <div class="col-xs-1 col-md-1 p-0">
                      <a href="https://www.edoome.com/" target="_blank"><i class="glyphicon glyphicon-info-sign"></i></a>
                    </div>
                  </div>                    
                  <p align="justify"><strong>Integración</strong> de sistemas de pagos <strong>PayU Latam</strong> para +15 países y optimización de <strong>Stripe</strong>. <strong>Desarrollo</strong> de <strong>herramientas</strong> para <strong>Engagement</strong> - estilo uber con códigos promocionales -, <strong>Creación de Horarios</strong> para Docentes, <strong>Sistema de Campañas Masivas</strong> con CronJobs, <strong>Landing Page</strong> nuevo para Edoome y Edoome Schools. <strong>Prospección</strong> y <strong>Calificación</strong> de clientes para versión Institucional y <strong>Mejora de Estrategia</strong> para conversión de clientes FREE a Pago.</p>
                </div>
              </li>
              <li>
                <div class="year pull-left">2017</div>
                <div class="description pull-right">
                  <div class="row">
                    <div class="col-xs-10 col-md-11">
                      <h3> GeekCode </h3>
                      <p align="justify">Desarrollador Full-Stack</p> 
                    </div>
                    <div class="col-xs-1 col-md-1 p-0">
                      <a href="javascript:;"><i class="glyphicon glyphicon-info-sign"></i></a>
                    </div>
                  </div>                    
                </div>
              </li>
              <li>
                <div class="year pull-left">2016</div>
                <div class="description pull-right">
                  <h3> Inventario Chile <a class="btn-link" href="http://inventario.nfconnection.cl/">www.inventario.nfconnection.cl</a></h3>
                  <p align="justify">Desarrollador Full-Stack</p>
                </div>
              </li>
              <li>
                <div class="year pull-left">2016</div>
                <div class="description pull-right">
                  <div class="row">
                    <div class="col-xs-10 col-md-11">
                      <h3>IDBand, Pulsera de Identificación </h3>
                      <p align="justify">Desarrollador Full-Stack</p>
                    </div>
                    <div class="col-xs-1 col-md-1 p-0">
                      <a href="https://youtu.be/9gph-83XGwo" target="_blank"><i class="glyphicon glyphicon-info-sign"></i></a>
                    </div>
                  </div> 
                </div>
              </li>
              <li>
                <div class="year pull-left">2015</div>
                <div class="description pull-right">
                  <h3>NFConnection LTDA <a class="btn-link" href="http://nfconnection.cl/">www.nfconnection.cl</a></h3>
                  <p align="justify">Fundador & CTO</p>
                </div>
              </li>
              <li>
                <div class="year pull-left"><img class="" src="" style="max-width: 80%;"/>2015</div>
                <div class="description pull-right">
                  <h3>Titulado de la Carrera Ingeniería en Informática</h3>
                  <p align="justify"></p>
                </div>
              </li>
              <li>
                <div class="year pull-left">2015</div>
                <div class="description pull-right">
                  <h3>Entel S.A.</h3>
                  <p align="justify">Desarrollador Full-Stack</p>
                </div>
              </li>
              <li>
                <div class="year pull-left">2014</div>
                <div class="description pull-right">
                  <h3>Proyecto Título</h3>
                  <p align="justify">Consistió, en controlar el acceso de puertas con Tecnología NFC, registrando el ingreso y salida de cada persona, que fueron alrededor de 45 a 50 alumnos de Ingeniería Informática, que participan en distintos proyectos de innovación en C.I.T.T.</p>
                </div>
              </li>
              <li>
                <div class="year pull-left">2014</div>
                <div class="description pull-right">
                  <h3>Competencia Robótica: “Carreras de Yale”, “Sumo” y “Tiro al Blanco”</h3>
                  <p align="justify"></p>
                </div>
              </li>
              <li>
                <div class="year pull-left">2013</div>
                <div class="description pull-right">
                  <h3> SmartHarvest</h3>
                  <p align="justify">Tester de aplicaciones Móviles y Desarrollador Front–End.</p>
                </div>
              </li>
              <li>
                <div class="year pull-left">2013</div>
                <div class="description pull-right">
                  <div class="row">
                    <div class="col-xs-10 col-md-11">
                      <h3> Amnistía Internacional</h3>
                      <p align="justify">Tester de aplicación Móvil.</p>
                    </div>
                    <div class="col-xs-1 col-md-1 p-0">
                      <a href="https://play.google.com/store/apps/details?id=cl.chihau.splash&hl=es" target="_blank"><i class="glyphicon glyphicon-info-sign"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>

        <div class="box">

          <h2>Conocimientos Especificos</h2>

          <div class="row f-1-2-em">
            <div class="col-md-4">
              <p class="font-1-1em"><strong>DevOps</strong></p>
              <ul>
                <li>EC2</li>
                <li>Lambda</li>
                <li>S3</li>
                <li>RDS</li>
                <li>SES</li>
                <li>IAM roles</li>
              </ul>
            </div>
            
            <div class="col-md-4">
              <p class="font-1-1em"><strong>Lenguaje de Programación</strong></p>
              <ul>
                <li>JAVA</li>
                <ul class="ml--30">
                  <li>JSP</li>
                  <li>JSF</li>
                </ul>
                
                <li>Python v2</li>
                <li>PHP POO</li>
                <li>JavaScript</li>
              </ul>
            </div>
            <div class="col-md-4">
              <p class="font-1-1em"><strong>Metodología Ágiles</strong></p>
              <ul>
                <li>Scrum</li>
              </ul>
            </div>
          </div>

          <div class="row f-1-2-em">
            <div class="col-md-4 ">
              <p class="font-1-1em"><strong>Frameworks</strong></p>
              <ul>
                <li>Angular +4 </li>
                <li>Ionic 2</li>
                <li>Bootstrap v2, v3, v4</li>
                <li>CodeIgniter v2</li>
                <li>Laravel v5.3, v5.4</li>
              </ul>
            </div>
            <div class="col-md-4">
              <p class="font-1-1em"><strong>Pasarelas de Pago</strong></p>
              <ul>
                <li>PayU Latam</li>
                <li>API Stripe</li>
                <li>API MercadoPago</li>
                <li>WebPay</li>
              </ul>
            </div>
            <div class="col-md-4">
              <p class="font-1-1em"><strong>Plan de mantenimiento</strong></p>
              <ul>
                <li>WorlClass</li>
              </ul>
            </div>
          </div>

          <div class="row f-1-2-em">
            <div class="col-md-4 ">
              <p class="font-1-1em"><strong>BD no Relacional</strong></p>
              <ul>
                <li>MongoDB</li>
              </ul>
            </div>
            <div class="col-md-4">
              <p class="font-1-1em"><strong>Móvil</strong></p>
              <ul>
                <li>Hibrido & Nativo, Android </li>
                <li>Hibrido IOS </li>
              </ul>
            </div>
            <div class="col-md-4">
              <p class="font-1-1em"><strong>Control de versiones</strong></p>
              <ul>
                <li>GIT </li>
              </ul>
            </div>
          </div>

          <div class="row f-1-2-em">
            <div class="col-md-4 ">
              <p class="font-1-1em"><strong>BD Relacional</strong></p>
              <ul>
                <li>MySQL</li>
                <li>MySQL Server</li>
                <li>PostgreSQL</li>
              </ul>
            </div>
            <div class="col-md-4">
              <p class="font-1-1em"><strong>Entornos de Ejecución</strong></p>
              <ul>
                <li>Node.js</li>
              </ul>
            </div>
            <div class="col-md-4">
              <p class="font-1-1em"><strong>Estructuras</strong></p>
              <ul>
                <li>Express.js</li>
              </ul>
            </div>
          </div>

        </div>
        
      </div>

      

      <div class="col-xs-12 col-sm-5">
        <!-- CV -->
        <div class="box">
          <h2>Curriculum Vitae</h2>
          <a class="btn btn-default btn-sm btn-block" href="pdf_cv/CV_VictorDiaz.pdf" download="Curriculum Vítae - Víctor Manuel Díaz Galdames.pdf" target="_blank"><i class="fa fa-download"></i> Descargar CV (.pdf)</a>
        </div>
        <!-- CONTACT -->
        <div class="box clearfix">
          <h2>Contacto</h2>
          <div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-phone fa-fw"></span></div>
              <div class="title only pull-right"><a href="tel:+56994919896">+56 9 9491 9896</a></div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-envelope fa-fw"></span></div>
              <div class="title only pull-right"><a href="mailto:victordiazgaldames@gmail.com" class="">victordiazgaldames@gmail.com</a></div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-home fa-fw"></span></div>
              <div class="title only pull-right">Residencia</div>
              <div class="description pull-right">Comuna de Santiago</div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-linkedin fa-fw"></span></div>
              <div class="title only pull-right"><a href="https://www.linkedin.com/profile/public-profile-settings?trk=prof-edit-edit-public_profile" target="_blank" class="">Perfil Linkedin</a></div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-twitter fa-fw"></span></div>
              <div class="title pull-right">Twitter</div>
              <div class="description pull-right"><a href="https://twitter.com/VictorMDiazG" target="_blank" class="">@VictorMDiazG</a></div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-facebook fa-fw"></span></div>
              <div class="title pull-right">Facebook</div>
              <div class="description pull-right"><a href="https://www.facebook.com/VictorMDiazGaldames/" target="_blank" class="">VictorMDiazGaldames</a></div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-skype fa-fw"></span></div>
              <div class="title pull-right">Skype</div>
              <div class="description pull-right">VictorMDiazGaldames</div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-github fa-fw"></span></div>
              <div class="title pull-right">GitHub</div>
              <div class="description pull-right"><a href="https://github.com/vdiazg" target="_blank" class="">github.com/vdiazg/</a></div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-bitbucket-square fa-fw"></span></div>
              <div class="title pull-right">Bitbucket</div>
              <div class="description pull-right"><a href="https://bitbucket.org/vdiazg/" target="_blank" class="">bitbucket.org/vdiazg/</a></div>
            </div>
            <div class="contact-item">
              <div class="icon pull-left text-center"><span class="fa fa-globe fa-fw"></span></div>
              <div class="title pull-right"><img src="img/chile.svg" class="text-left img-responsive" width="" height="" style="max-height: 100%; height: 41px;" alt=""></div>
              <!-- <div class="description pull-right"><a href="https://bitbucket.org/vdiazg/" target="_blank" class="">https://bitbucket.org/vdiazg/</a></div> -->
            </div>
          </div>
        </div>
        <!-- SKILLS -->
        <div class="box">
          <h2>Full-Stack</h2>
          <div class="skills">
            <div class="item-skills" data-percent="1.00">Entorno de servidor cloud, red y hosting</div>
            <div class="item-skills" data-percent="1.00">Entendimiento de las necesidades del negocio y del cliente</div>
            <div class="item-skills" data-percent="1.00">Modelo de Datos</div>
            <div class="item-skills" data-percent="1.00">Lógica de Negocio</div>
            <div class="item-skills" data-percent="1.00">Experiencia de Usuario</div>
            <div class="item-skills" data-percent="1.00">API REST: Laravel +5.4</div>
            <div class="item-skills" data-percent="1.00">WorkFlows - GIT</div>

            <h2>Marcos de Desarrollo</h2>
            
            <div class="skills-legend clearfix">
              <div class="legend-left legend">Novato</div>
              <div class="legend-left legend"><span>Competente</span></div>
              <div class="legend-right legend"><span>Maestro</span></div>
              <div class="legend-right legend">Experto</div>
            </div>
            <div class="item-skills" data-percent="0.70">Scrum</div>
          </div>
        </div>   
        <!-- REFERENCES -->
        <div class="box">
          <h2>Referencias</h2>
          <div  style="">
            <div class="job clearfix">
              <div class="col-xs-4">
                <div class="where">Wybe Terband</div>
              </div>
              <div class="col-xs-8">
                <div class="profession">CMO<br><span class="email-contact"><span class="fa fa-envelope fa-fw contact-item-custom"></span> wybe@edoome.com</span><br><span class="email-contact"><span class="fa fa-phone fa-fw contact-item-custom"></span> +56 9 9020 8066 <br><span class="name-company">Edoome Inc.</span></div>
                <div class="contact-item">
                </div>
              </div>
            </div>
            <div class="job clearfix">
              <div class="col-xs-4">
                <div class="where">Sebastian de la Fuente</div>
              </div>
              <div class="col-xs-8">
                <div class="profession">CTO <br><span class="email-contact"><span class="fa fa-envelope fa-fw contact-item-custom"></span> sdelafuente@edoome.com</span><br><span class="email-contact"><span class="fa fa-phone fa-fw contact-item-custom"></span> +56 9 8186 6739 <br><span class="name-company">Edoome Inc.</span></div>
                <div class="contact-item">
                </div>
              </div>
            </div>
            <div class="job clearfix">
              <div class="col-xs-4">
                <div class="where">Leonardo de la Fuente</div>
              </div>
              <div class="col-xs-8">
                <div class="profession">CEO <br><span class="email-contact"><span class="fa fa-envelope fa-fw contact-item-custom"></span> ldelafuente@edoome.com</span><br><span class="email-contact"><span class="fa fa-phone fa-fw contact-item-custom"></span> +56 9 6596 0374 <br><span class="name-company">Edoome Inc.</span></div>
                <div class="contact-item">
                </div>
              </div>
            </div>
            <div class="job clearfix">
              <div class="col-xs-4">
                <div class="where">Ignacio Navarrete </div>
              </div>
              <div class="col-xs-8">
                <div class="profession">CEO & CMO <br><span class="email-contact"><span class="fa fa-envelope fa-fw contact-item-custom"></span> ignacio.navarrete@gmail.com</span><br><span class="email-contact"><span class="fa fa-phone fa-fw contact-item-custom"></span> +56 9 9392 0829 <br><span class="name-company">NFConnection LTDA.</span></div>
                <div class="contact-item">
                </div>
              </div>
            </div>
            <div class="job clearfix">
              <div class="col-xs-4">
                <div class="where">Marta Navarro</div>
              </div>
              <div class="col-xs-8">
                <div class="profession">CEO & CTO <br><span class="email-contact"><span class="fa fa-envelope fa-fw contact-item-custom"></span> marta.navarro@nfconnection.cl</span><br><span class="email-contact"><span class="fa fa-phone fa-fw contact-item-custom"></span> +56 9 7774 0488 <br><span class="name-company">NFConnection LTDA.</span></div>
                <div class="contact-item">
                  <!-- <button type="button" class="btn btn-default btn-sm" title="Contacto Telefónico" data-container="body" data-toggle="popover" data-placement="right" data-content="(+56 9) 77740488"><span class="fa fa-phone fa-fw contact-item-custom"></span></button>
                    <button type="button" class="btn btn-default btn-sm" title="Correo Electrónico" data-container="body" data-toggle="popover" data-placement="right" data-content="marta.navarro@nfconnection.cl"><span class="fa fa-envelope fa-fw contact-item-custom"></span></button> -->
                  </div>
                </div>
              </div>
            </div>
          </div>   
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <!-- PROJECTS -->
          <div class="box">
            <h2>Proyectos</h2>
            <div class="row">
              <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-heading" style="">
                    <div class="row">
                      <div class="col-xs-9">
                        <h3 class="panel-title">Software de Acreditación</h3>
                      </div>
                      <div class="col-xs-3">
                        <img src="https://vignette.wikia.nocookie.net/spore/images/2/2e/Java_Logo.svg" class="text-left img-circle img-responsive" width="" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="panel-body p-0">
                    <img src="./img/images_proyects/7.jpg" class="img-responsive" alt="...">
                  </div>
                  <div class="panel-footer">
                    <p><b>Descripción</b></p>
                    <p align="justify">Consistió en desarrollar un Software para acreditar a más de 350 personas, utilizando la tecnología NFC mediante el uso de Tarjetas Personalizadas.</p>
                    <p><b>Stack Utilizado</b></p>
                    <ul class="ml--30">
                      <div class="row">
                        <div class="col-md-6">
                          <li>NetBeans 8.2</li>
                          <li>MySQL 5.0</li>
                        </div>
                        <div class="col-md-6">
                          <li>Java Card</li>
                        </div>                          
                      </div>
                    </ul>
<!--                     <div class="row">
                      <div class="col-md-12">
                        <a href="javascript:;" class="btn btn-block btn-social btn-bitbucket"><i class="fa fa-bitbucket"></i> Ver Código</a>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-heading" style="">
                    <div class="row">
                      <div class="col-xs-9">
                        <h3 class="panel-title">Software de Control de Acceso</h3>
                      </div>
                      <div class="col-xs-3">
                        <img src="https://vignette.wikia.nocookie.net/spore/images/2/2e/Java_Logo.svg" class="text-left img-circle img-responsive" width="" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="panel-body p-0">
                    <img src="./img/images_proyects/control_access.jpg" class="img-responsive" alt="...">
                  </div>
                  <div class="panel-footer">
                    <p><b>Descripción</b></p>
                    <p align="justify">Consistió en desarrollar un software para controlar el acceso de alrededor de 50 alumnos del Instituto Profesional DuocUC, Sede Viña del Mar.</p>
                    <p><b>Stack Utilizado</b></p>
                    <ul class="ml--30">
                      <div class="row">
                        <div class="col-md-6">
                          <li>NetBeans 8.2</li>
                          <li>MySQL 5.0</li>
                        </div>
                        <div class="col-md-6">
                          <li>Java Card</li>
                          
                          
                        </div>                          
                      </div>
                    </ul>
<!--                     <div class="row">
                      <div class="col-md-12">
                        <a href="javascript:;" class="btn btn-block btn-social btn-bitbucket"><i class="fa fa-bitbucket"></i> Ver Código</a>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-heading" style="">
                    <div class="row">
                      <div class="col-xs-9">
                        <h3 class="panel-title">Blog</h3>
                      </div>
                      <div class="col-xs-3">
                        <img src="https://d301sr5gafysq2.cloudfront.net/f01423d08abc/img/repo-avatars/php.svg" class="text-left img-circle img-responsive" width="" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="panel-body p-0">
                    <img src="./img/images_proyects/blog.png" class="img-responsive" alt="...">
                  </div>

                  <div class="panel-footer">
                    <p><b>Descripción</b></p>
                    <p align="justify">
                      Desarrollo Front–End y Back-End en PHP de plataforma Web para profundizar conocimiento en Framework Laravel 5.3 y Paypal.
                    </p>
                    <p><b>Stack Utilizado</b></p>
                    <ul class="ml--30">
                      <div class="row">
                        <div class="col-md-6">
                          <li>Laravel 5.4</li>
                          <li>MySQL 5.0</li>
                          <li>Apache/PHP 5.6.28</li>
                          <li>Bootstrap v3.3.7</li>
                        </div>
                        <div class="col-md-6">
                          <li>Manipulación de fechas con Carbon</li>
                          <li>Choose (JQuery)</li>
                          <li>Trumbowyg (JQuery)</li>
                        </div>                          
                      </div>
                    </ul>
<!--                     <div class="row">
                      <div class="col-md-12">
                        <a href="javascript:;" class="btn btn-block btn-social btn-bitbucket"><i class="fa fa-bitbucket"></i> Ver Código</a>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-heading" style="">
                    <div class="row">
                      <div class="col-xs-9">
                        <h3 class="panel-title">Plataforma OSS-RC.</h3>
                        <!-- de gestión OSS-RC e inventario de la red móvil -->
                      </div>
                      <div class="col-xs-3">
                        <img src="https://d301sr5gafysq2.cloudfront.net/f01423d08abc/img/repo-avatars/php.svg" class="text-left img-circle img-responsive" width="" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="panel-body p-0">
                    <img src="./img/images_proyects/inventario_entel.png" class="img-responsive" alt="...">
                  </div>

                  <div class="panel-footer">
                    <p><b>Descripción</b></p>
                    <p align="justify">
                      Desarrollo Front-End y Back-End en PHP del Panel de Control de la Red
                      Móvil. Incorporando el estado de salud de plataformas de gestión OSS-RC
                    e inventario de la red móvil. </p>
                    <p><b>Stack Utilizado</b></p>
                    <ul class="ml--30">
                      <div class="row">
                        <div class="col-md-6">
                          <li>Html5</li>
                          <li>Apache/PHP 5.6.28</li>
                          <li>MySQL 5.0</li>
                          <li>DatePicker (JQuery)</li>
                          <li>dropzone (JQuery)</li>
                          <li>DataTables (JQuery)</li>
                        </div>
                        <div class="col-md-6">
                          <li>Gráficos Morris & Rapheal (JQuery)</li>
                          <li>Bootstrap v3.2.0</li>
                          <li>Font Awesome 4.2.0</li>
                          <li>JQueryValidate</li>
                          <li>Estructura MVC</li>
                        </div>
                      </div>
                      
                      
                    </ul>
<!--                     <div class="row">
                      <div class="col-md-12">
                        <a href="javascript:;" class="btn btn-block btn-social btn-bitbucket"><i class="fa fa-bitbucket"></i> Ver Código</a>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="row">
                      <div class="col-xs-9">
                        <h3 class="panel-title">Curriculum Vítae</h3>
                      </div>
                      <div class="col-xs-3">
                        <img src="https://d301sr5gafysq2.cloudfront.net/f01423d08abc/img/repo-avatars/php.svg" class="text-left img-circle img-responsive" width="" alt="">
                      </div>
                    </div>

                  </div>
                  <div class="panel-body p-0">
                    <img src="./img/images_proyects/cv.png" class="img-responsive" alt="...">
                  </div>

                  <div class="panel-footer">
                    <p><b>Descripción</b></p>
                    <p align="justify">Sitio web que da a concer habilidades y conocimientos de una forma mas dínamica.</p>
                    <p><b>Stack Utilizado</b></p>
                    <ul class="ml--30">
                      <div class="row">
                        <div class="col-md-6">
                          <li>Html5</li>
                          <li>Apache/PHP 5.6.28</li>
                          <li>PHPMailer 5.2.21</li>
                          <li>Bootstrap v3.3.7</li>
                        </div>
                        <div class="col-md-6">
                          <li>Font Awesome 4.2.0</li>
                          <li>JQueryValidate</li>
                          <li>Estructura MVC</li>
                        </div>
                      </div>
                      
                      
                    </ul>
<!--                     <div class="row">
                      <div class="col-md-12">
                        <a href="javascript:;" class="btn btn-block btn-social btn-bitbucket"><i class="fa fa-bitbucket"></i> Ver Código</a>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
              
              <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-heading" style="">
                    <div class="row">
                      <div class="col-xs-9">
                        <h3 class="panel-title">InventarioChile</h3>
                      </div>
                      <div class="col-xs-3">
                        <img src="https://d301sr5gafysq2.cloudfront.net/f01423d08abc/img/repo-avatars/php.svg" class="text-left img-circle img-responsive" width="" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="panel-body p-0">
                    <img src="./img/images_proyects/inventarioChile.jpg" class="img-responsive" alt="...">
                  </div>
                  <div class="panel-footer">
                    <p><b>Descripción</b></p>
                    <p align="justify">Plataforma Saas que permite gestionar Productos, Sotck, Bodegas, Usuarios y Ordenes de Despacho y visualización de gráficos.</p>
                    <!-- - Manejo de Productos, Sotck, Bodegas, Usuarios y Ordenes de Despacho. -->
                    <p><b>Stack Utilizado</b></p>
                    <ul class="ml--30">
                      <div class="row">
                        <div class="col-md-6">
                          <li>Html5</li>
                          <li>Apache/PHP 5.6.28</li>
                          <li>MySQL 5.0</li>
                          <li>DatePicker (JQuery)</li>
                          <li>DataTables (JQuery)</li>
                        </div>
                        <div class="col-md-6">
                          <li>Gráficos Morris & Rapheal (JQuery)</li>
                          <li>Bootstrap v3.2.0</li>
                          <li>Font Awesome 4.2.0</li>
                          <li>JQueryValidate</li>
                          <li>Estructura MVC</li>
                        </div>
                      </div>
                    </ul>
<!--                     <div class="row">
                      <div class="col-md-12">
                        <a href="javascript:;" class="btn btn-block btn-social btn-bitbucket"><i class="fa fa-bitbucket"></i> Ver Código</a>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="panel panel-default">
                  <div class="panel-heading" style="">
                    <div class="row">
                      <div class="col-xs-9">
                        <h3 class="panel-title">GeekCode</h3>
                      </div>
                      <div class="col-xs-3">
                        <img src="https://d301sr5gafysq2.cloudfront.net/f01423d08abc/img/repo-avatars/php.svg" class="text-left img-circle img-responsive" width="" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="panel-body p-0">
                    <img src="./img/images_proyects/geekcode_logo.png" class="img-responsive" alt="...">
                  </div>

                  <div class="panel-footer">
                    <p><b>Descripción</b></p>
                    <p align="justify">Desarrollo de plataforma Saas, que permite reunir a los Desarrolladores de Chile en un solo lugar, compartiendo contenido y posicionandose según sus habilidades.</p>
                    <p><b>Stack Utilizado</b></p>
                    <ul class="ml--30">
                      <div class="row">
                        <div class="col-md-6">
                          <li>Laravel 5.4</li>
                          <li>MySQL 5.0</li>
                          <li>Apache/PHP 5.6.28</li>
                          <li>Bootstrap v3.3.7</li>
                        </div>
                        <div class="col-md-6">
                          <li>Manipulación de fechas con Carbon</li>
                          <li>Choose (JQuery)</li>
                          <li>Trumbowyg (JQuery)</li>
                        </div>                          
                      </div>
                    </ul>
<!--                     <div class="row">
                      <div class="col-md-12">
                        <a href="javascript:;" class="btn btn-block btn-social btn-bitbucket"><i class="fa fa-bitbucket"></i> Ver Código</a>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <h2>Ultimos Cursos Realizados</h2>
            <div class="row">
              <div class="col-md-3">
               <a class="border-line" href="https://www.udemy.com/angular-2-fernando-herrera/learn/v4/overview" target="_blank">
                <div class="panel panel-default">

                  <div class="panel-heading" style="">
                    <h5 style="font-size: 13px;" class="panel-title">De cero a experto creando aplicaciones (Angular 5+)</h5> 
                  </div>

                  <div class="panel-body p-0">
                    <img src="https://udemy-images.udemy.com/course/480x270/1075334_8b5f_4.jpg" class="img-responsive" alt="...">
                  </div>

                  <div class="panel-footer">
                    <!-- <h6></h6> -->
                    <ul class="ml--25">
                      <li>Firebase</li>
                      <li>Firebase RESTful services</li>
                      <li>TypeScript</li>
                      <li>ECMAScript 6</li>
                      <li>Reactive-Extensions</li>
                      <li>Sockets</li>
                      <li>Ionic 2</li>
                      <li>Ionic view – para realizar pruebas en dispositivos</li>
                      <li>Atom packages</li>
                      <li>Angular CLI</li>
                      <li>AngularFire2</li>
                      <li>Local Storage</li>
                      <li>Bootstrap 4</li>
                      <li>Spotify API</li>
                      <li>Youtube API</li>
                      <li>Entre otras tecnologías…</li>
                    </ul>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-3">
             <a class="border-line" href="https://www.udemy.com/api-restful-con-laravel-php-homestead-passport/learn/v4/overview" target="_blank">
              <div class="panel panel-default">

                <div class="panel-heading" style="">
                  <h5 style="font-size: 13px;" class="panel-title">API RESTful con Laravel: Guía Definitiva</h5>
                </div>

                <div class="panel-body p-0">
                  <img src="https://udemy-images.udemy.com/course/480x270/1109106_d644_4.jpg" class="img-responsive" alt="...">
                </div>

                <div class="panel-footer">
                  <!-- <h6></h6> -->
                  <ul class="ml--25">
                    <li><strong>Cacheo </strong>de respuestas.</li>
                    <li>Limitación de peticiones con middlewares</li>
                    <li>Implementación&nbsp;de <strong>HATEOAS </strong></li>
                    <li><strong>PHP Fractal</strong></li>
                    <li>Cabeceras <strong>CORS </strong>en la API para permitir el uso desde JavaScript y navegadores web</li>
                    <li><strong> Policies</strong> y <strong>Gates </strong>para controlar el acceso de los usuarios a los recursos de la API&nbsp;RESTful</li>
                  </ul>
                </div>
              </div>
            </a>
          </div>
          <div class="col-md-3">
           <a class="border-line" href="https://codigofacilito.com/cursos/ecommerce-php" target="_blank">
            <div class="panel panel-default">

              <div class="panel-heading" style="">
                <h5 style="font-size: 13px;" class="panel-title">Tienda en Linea con PHP y Laravel</h5>
              </div>
              <div class="panel-body p-0" style="background-color:#0d47a1; text-align: center;">
                <img style="max-height: 107px; height:100%; z-index:10" class="relative avatar" src="https://codigofacilito.com/system/courses/white_avatars/000/000/103/medium/php-ecommerce.png" alt="Php ecommerce">
              </div>

              <div class="panel-footer">
                <!-- <h6></h6> -->
                <ul class="ml--25">
                  <li>REST</li>
                  <li>Migraciones</li>
                  <li>API PayPal</li>
                  <li>Middlewares</li>
                  <li>Ajax</li>
                </ul>
              </div>
            </div>
          </a>
        </div>
           <!--  <div class="skills">

            
            <a href="https://www.udemy.com/angular-2-fernando-herrera/learn/v4/overview" target="_blank" class="item-skills" data-percent="1.00">Angular: De cero a experto creando aplicaciones (Angular 5+)</a>
            <a href="#" class="item-skills" data-percent="1.00">Prueba</a>
            <a href="#" class="item-skills" data-percent="1.00">Prueba</a>
          </div>  --> 
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <!-- LANGUAGES -->
      <div class="box">
        <h2>Idiomas</h2>
        <div id="language-skills">
          <div class="skill">Inglés <div class="icons pull-right"><div style="width: 40%;" class="icons-red"></div></div></div>
        </div>
      </div>
    </div>
      <!-- <div class="col-md-4">
        <div class="box">
          <h2>Conversemos!</h2>
          <form id="JSV_Hablemos" method="POST">
            <div class="form-group">
              <label for="InputName">Ingrese tu nombre</label>
              <input type="text" class="form-control" id="InputName" name="InputName" placeholder="" required="">
            </div>
            <div class="form-group">
              <label for="InputEmail">Ingrese tu email</label>
              <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="" required="">
            </div>
            <div class="form-group">
              <label for="InputMsg">Ingrese tu mensaje</label>
              <textarea class="form-control" id="InputMsg" name="InputMsg" rows="5"></textarea>
            </div>
            <div class="row">
              <div class="col-md-2"><button type="submit" class="btn btn-default btn-success">Enviar</button></div>
              <div class="col-md-10">
                <div id="msj_respuesta"></div>
              </div>
            </div>
          </form>
        </div>
      </div> -->
      <div class="col-md-6">
        <div class="box">
          <h2>Frases que me identifican!</h2>
          <blockquote>
            <p>La Innovación es lo que distingue a un Líder de los demás.</p>
            <small>Frase célebre de <cite title="Steve Jobs">Steve Jobs</cite></small>
          </blockquote>
        </div>
      </div>
    </div>
    <div class="row">
      <hr>
      <div class="col-xs-12">
        <footer>
          <p class="text-center">© 2017 GeekCode, Todos los Derechos Reservados. <a href="mailto:contacto@geekcode.cl">victordiazgaldames@gmail.com</a>.</p>
        </footer>

      </div>
    </div>
    <div class="">
      <?php 
      // require('./modals/project-blog.php');
      //  require('./modals/project-cv.php'); 
      // require('./modals/project-entel.php');
      // require('./modals/project-inventoryChile.php');
      // require('./modals/project-geekCode.php'); ?>
    </div>
  </div>
  
  <!-- JQUERY -->
  <script src="js/jquery.min.js"></script>
  <!-- BOOTSTRAP -->
  <script src="js/bootstrap.min.js"></script>
  <!-- SCRIPTS -->
  <script src="js/scripts.js"></script>
  <!-- <script src="js/JQueryValidate\jquery.validate.min.js"></script>
  <script src="js/JQueryValidate\CustomJQueryValidate\custom_jquery_validate.js"></script>
  <script src="js\JQueryValidate\Validates\JS.Validate.Hablemos.js"></script> -->
  <!-- scripts -->
  <script src="node_modules/particles.js/particles.js"></script>
  <script src="node_modules/particles.js/demo/js/app.js"></script>

  <script type="text/javascript">

    function getMessge(text) {

      //Splitting it with : as the separator
      var inputText = text.split(" ");

      var OutputText = '';
      $(inputText).each(function( i, v ) {
        OutputText = OutputText + v.charAt(0);
      });

        //Then read the values from the array where 0 is the first
        return OutputText;
      }

      function getLeast(line, givenNumber) {
      // escribe tu respuesta aquí
      var inputText = line.split(" ");

      var array1 = [];
      var min;

      var result;

      $(inputText).each(function( i, line ) {

        if(line > givenNumber && line < 1000)
        {
          array1.push(line);
        }
      });

      if(array1.length == 0)
      {
        min = '-1';
      }
      else
      {
        min = Math.min.apply(null, array1);
      }
      
      return min;

    }

    // Considere x de tipo array y p de tipo entero
    function fractile(x, p) {
      // Ingresa tu respuesta aquí

    }
    
    console.log(fractile("{1,2,3,4,5,6,7,8,9,10}",39));
  </script>
</body>
</html>
