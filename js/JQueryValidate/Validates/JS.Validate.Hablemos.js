$("#JSV_Hablemos").validate({
    rules: {
        InputName: {required: true},
        InputEmail: {required: true, validacion_email: true},
        InputMsg: {required: true}
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    errorClass: 'help-block',
    messages: {},
    submitHandler: function (form) {
        $('#msj_respuesta').html('<div><img src="./img/ajax-loader.gif" alt="" /> enviando mensaje...</div>');
        $.ajax({
            type: "POST",
            url: "./controller/MOD.Contact.Hablemos.php",
            data: $(form).serialize(),
            success: function (data) {
              $('#msj_respuesta').html(data);
              form.reset();
            }
        });

    }
});
