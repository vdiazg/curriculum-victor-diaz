// Para sobreescribir los mensajes de error de jquery-validation-plugin
jQuery.extend(jQuery.validator.messages, {
    required: "Debe completar este campo.",
    remote: "Please fix this field.",
    email: "Correo eletrónico es inválido.",
    url: "Please enter a valid URL.",
    date: "Por favor, introduzca una fecha válida.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Debe ingresar un número válido.",
    digits: "Debe ingresar números enteros (dí­gitos).",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Debe ingresar la misma contraseña nuevamente.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Largo maximo del campo: {0}."),
    minlength: jQuery.validator.format("Largo minimo del campo: {0}."),
    rangelength: jQuery.validator.format("Debe ingresar un valor con un largo entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Debe ingresar un valor menor o igual a {0}."),
    min: jQuery.validator.format("Debe ingresar un valor mayor o igual a {0}.")
});

// Para validar un rut Chileno con Jquery-Validate
function validaRut(campo) {
    if (campo.length == 0) {
        return false;
    }
    if (campo.length < 8) {
        return false;
    }

    // Chequea que tenga el guion antes del digito verificador
    if (campo.charAt(campo.length - 2) != '-') {
        return false;
    }

    // Chequea que no tenga puntos (esto se puede quitar si deseas que el rut contenga puntos)
    if (campo.indexOf('.') != -1) {
        return false;
    }

    campo = campo.replace('-', '')
    campo = campo.replace(/\./g, '')

    var suma = 0;
    var caracteres = "1234567890kK";
    var contador = 0;
    for (var i = 0; i < campo.length; i++) {
        u = campo.substring(i, i + 1);
        if (caracteres.indexOf(u) != -1)
            contador++;
    }
    if (contador == 0) {
        return false
    }

    var rut = campo.substring(0, campo.length - 1)
    var drut = campo.substring(campo.length - 1)
    var dvr = '0';
    var mul = 2;

    for (i = rut.length - 1; i >= 0; i--) {
        suma = suma + rut.charAt(i) * mul
        if (mul == 7)
            mul = 2
        else
            mul++
    }
    res = suma % 11
    if (res == 1)
        dvr = 'k'
    else if (res == 0)
        dvr = '0'
    else {
        dvi = 11 - res
        dvr = dvi + ""
    }
    if (dvr != drut.toLowerCase()) {
        return false;
    }
    else {
        return true;
    }
}

$.validator.addMethod("rut_required", function (value, element) {
    return this.optional(element) || validaRut(value);
}, "El rut ingresado es inválido.");

$.validator.addMethod("letras_espacios_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-ZáéíóúñÁÉÍÓÚ ]+" + "$"));
}, "En este campo sólo se permiten letras y espacios.");

$.validator.addMethod("letras_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-ZáéíóúñÁÉÍÓÚ]+" + "$"));
}, "En este campo sólo se permiten letras");

$.validator.addMethod("letras_numeros_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-z0-9]+" + "$"));
}, "En este campo sólo se permiten letras minúsculas y números.");

$.validator.addMethod("letras_numeros_espacios_guion_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-Z- 0-9]+" + "$"));
}, "En este campo sólo se permiten letras mayusculas, minusculas, guiones y números.");

$.validator.addMethod("letras_espacios_comas_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-Z, ]+" + "$"));
}, "En este campo sólo se permiten letras mayusculas, minusculas y comas.");

$.validator.addMethod("dos_decimales_required", function (value, element) {
    return value.match(new RegExp("^([0-9]+)|([0-9]+\.([0-9]|[0-9][0-9]))$"));
}, "En este campo debe ingresar un número con 1 o 2 decimales.");

$.validator.addMethod("letras_espacios_puntos_required", function (value, element) {
    return value.match(new RegExp("^" + "[a-zA-ZáéíóúñÁÉÍÓÚ .]+" + "$"));
});

$.validator.addMethod("validacion_email", function (value, element) {
    return value.match(new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i));
}, "Correo eletrónico es inválido.");

$.validator.addMethod("numeros_letras_guion", function (value, element) {
    return value.match(new RegExp(/^[0-9-]{1,10}$/));
});

$.validator.addMethod("numeros_required", function (value, element) {
    return value.match(new RegExp("^" + "[0-9]+" + "$"));
}, "En este campo sólo se permiten números.");

$.validator.addMethod('IP4Checker', function (value) {
    return value.match(new RegExp(/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/));
}, 'Dirección IP Inválida');

$.validator.addMethod('url_validation', function (value) {
    return value.match(new RegExp(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/));
}, 'URL Inválida');

$.validator.addMethod('numero_celular_chileno', function (value) {
    return value.match(new RegExp("^[+][0-9]{3,3}-? ?[0-9]{8,8}$"));
}, 'Número inválido, Formato Correcto: +56912345678');

$(document).ready(function () {


});
