<?php
//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../vendor/autoload.php';

// Instantiate a new PHPMailer 
$mail = new PHPMailer;
$mail->SMTPDebug = 2; 
// Tell PHPMailer to use SMTP
$mail->isSMTP();

// Replace sender@example.com with your "From" address. 
// This address must be verified with Amazon SES.
$mail->setFrom('no-reply@nfconnection.cl', 'Sender Name');

// Replace recipient@example.com with a "To" address. If your account 
// is still in the sandbox, this address must be verified.
// Also note that you can include several addAddress() lines to send
// email to multiple recipients.
$mail->addAddress('victordiazgaldames@gmail.com', 'Recipient Name');

// Replace smtp_username with your Amazon SES SMTP user name.
$mail->Username = 'no-reply';

// Replace smtp_password with your Amazon SES SMTP password.
$mail->Password = 'sapo.2957053';
    
// Specify a configuration set. If you do not want to use a configuration
// set, comment or remove the next line.
$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
 
// If you're using Amazon SES in a region other than US West (Oregon), 
// replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
// endpoint in the appropriate region.
$mail->Host = 'email-smtp.us-west-2.amazonaws.com';

// The subject line of the email
$mail->Subject = 'Amazon SES test (SMTP interface accessed using PHP)';

// The HTML-formatted body of the email
$mail->Body = '<h1>Email Test</h1>
    <p>This email was sent through the 
    <a href="https://aws.amazon.com/ses">Amazon SES</a> SMTP
    interface using the <a href="https://github.com/PHPMailer/PHPMailer">
    PHPMailer</a> class.</p>';

// Tells PHPMailer to use SMTP authentication
$mail->SMTPAuth = true;

// Enable TLS encryption over port 587
$mail->SMTPSecure = 'tls';
$mail->Port = 587;

// Tells PHPMailer to send HTML-formatted email
$mail->isHTML(true);

// The alternative email body; this is only displayed when a recipient
// opens the email in a non-HTML email client. The \r\n represents a 
// line break.
$mail->AltBody = "Email Test\r\nThis email was sent through the 
    Amazon SES SMTP interface using the PHPMailer class.";

if(!$mail->send()) {
    echo "Email not sent. " , $mail->ErrorInfo , PHP_EOL;
} else {
    echo "Email sent!" , PHP_EOL;
}

// $InputName = trim(filter_input(INPUT_POST, 'InputName'));
// if (empty($InputName)) { echo '<div class="alert alert-danger"><strong> * Debe Ingresar su nombre. </strong></div>'; return; }
// $InputEmail = trim(filter_input(INPUT_POST, 'InputEmail'));
// if (empty($InputEmail)) { echo '<div class="alert alert-danger"><strong> * Debe Ingresar su email. </strong></div>'; return; }
// $InputMsg = trim(filter_input(INPUT_POST, 'InputMsg'));
// if (empty($InputMsg)) { echo '<div class="alert alert-danger"><strong> * Debe Ingresar el mensaje. </strong></div>'; return; }


// $mail = new PHPMailer;

// $mail->SMTPOptions = array(
// 'ssl' => array(
// 'verify_peer' => false,
// 'verify_peer_name' => false,
// 'allow_self_signed' => true
// )
// );
// //Server settings
// $mail->SMTPDebug = 2;                                 // Enable verbose debug output
// $mail->isSMTP();                                      // Set mailer to use SMTP
// $mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
// $mail->Host = 'email-smtp.us-west-2.amazonaws.com';  // Specify main and backup SMTP servers ; smtp.mail.us-east-1.awsapps.com; imap.mail.us-west-2.awsapps.com; smtp.mail.eu-west-1.awsapps.com;
// $mail->SMTPAuth = true;                               // Enable SMTP authentication
// $mail->Username = 'no-reply';                 // SMTP username
// $mail->Password = 'sapo.2957053';                           // SMTP password
// $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
// $mail->Port = 587;                                    // TCP port to connect to

// // print_r($mail); exit();

// $mail->setFrom('no-reply@nfconnection.cl', $InputName.' (a través de NFConnection)');
// $mail->addAddress("victordiazgaldames@gmail.com", "Víctor Díaz");     // Add a recipient
// $mail->addReplyTo($InputEmail, $InputName);
// $mail->CharSet = 'UTF-8';
// $mail->isHTML(true);                                  // Set email format to HTML
// $mail->Subject = 'Ya estamos en Contacto !';

// $postdata = http_build_query(
//   array(
//     'var1' => $InputName,
//     'var2' => $InputEmail,
//     'var3' => $InputMsg
//   )
// );

// $opts = array('http' =>
//   array(
//     'method'  => 'POST',
//     'header'  => 'Content-type: application/x-www-form-urlencoded',
//     'content' => $postdata
//   )
// );

// $context  = stream_context_create($opts);

// $body = file_get_contents('http://geekcode.cl/victordiaz/EmailTemplatesHtml/de.php', false, $context);
// //$body             = file_get_contents('http://victordiaz.nfconnection.cl/EmailTemplatesHtml/de.php');
// $body             = eregi_replace("[\]",'',$body);
// $mail->MsgHTML($body);

// if($mail->send()) {
//   //echo 'Mensaje enviado con exito';
//   $mail->clearAddresses();
//   $mail->clearCCs();
//   $mail->clearBCCs();
//   $mail->clearReplyTos();
//   $mail->clearAllRecipients();
//   $mail->clearAttachments();
//   $mail->clearCustomHeaders();

//   $mail->setFrom('no-reply@nfconnection.cl', 'Víctor Díaz (a través de NFConnection)');
//   $mail->addAddress($InputEmail, $InputName);      // Add a recipient
//   $mail->addReplyTo("victordiazgaldames@gmail.com", "Víctor Díaz");
//   $mail->CharSet = 'UTF-8';
//   $mail->isHTML(true);                                  // Set email format to HTML
//   $mail->Subject = 'Ya estamos en Contacto !';
//   $postdata = http_build_query(
//     array(
//       'InputName' => $InputName,
//       'InputEmail' => $InputEmail,
//       'InputMsg' => $InputMsg
//     )
//   );

//   $opts = array('http' =>
//     array(
//       'method'  => 'POST',
//       'header'  => 'Content-type: application/x-www-form-urlencoded',
//       'content' => $postdata
//     )
//   );

//   $context  = stream_context_create($opts);

//   $body = file_get_contents('http://geekcode.cl/victordiaz/EmailTemplatesHtml/para.php', false, $context);

// //$body             = file_get_contents('http://victordiaz.nfconnection.cl/EmailTemplatesHtml/para.php');
//   $body             = eregi_replace("[\]",'',$body);

//   $mail->MsgHTML($body);

//   if($mail->send())
//   {
//     echo '<div class="alert alert-success"><strong> * Ya estamos en contacto !. </strong></div>';
//   }
//   else
//   {
//     echo '<div class="alert alert-danger"><strong> * 2. Error en enviar mensaje, intenta más tarde. </strong></div>';
//   }
// }else
// {
//   echo '<div class="alert alert-danger"><strong> * 1. Error en enviar mensaje, intenta más tarde. </strong></div>';
// }
?>
